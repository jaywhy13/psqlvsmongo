from django.core.management.base import BaseCommand

from psqlvsmongo.models import run_timing

class Command(BaseCommand):
    help = "Run timing"

    def handle(self, *args, **options):
    	run_timing()
