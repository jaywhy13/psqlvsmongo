import time
import json
import random

from django.contrib.gis.db import models
from django.contrib.gis.gdal import DataSource
from mongoengine import *
from django.conf import settings

# Top Left: 18.37537909403189, -77.662353515625
# Top Right: 18.216307167661874, -76.73675537109375
# Bottom Right: 17.986570330361708, -76.61041259765625
# Bottom Left: 17.976120606637693, -77.70904541015625

class PsqlRoad(models.Model):

	name = models.CharField(max_length=255, blank=True, null=True, db_index=True)
	geom = models.MultiLineStringField(blank=True, null=True)

	objects = models.GeoManager()

class MongoRoad(Document):

    name = StringField(max_length=200)
    geom = LineStringField()


class PsqlPolygon(models.Model):

	name = models.CharField(max_length=255, blank=True, null=True, db_index=True)
	geom = models.MultiPolygonField(blank=True, null=True)

	objects = models.GeoManager()


class MongoPolygon(Document):

    name = StringField(max_length=200)
    geom = PolygonField()

def time_geo(model, is_mongo=False, points=2500, f=None):
	if is_mongo:
		print "Timing mongo with %s points" % points
	else:
		print "Timing Postgis with %s points" % points

	lookup_time = 0
	for i in range(points):
		lat = random.uniform(18.37537909403189, 17.986570330361708)
		lon = random.uniform(-77.662353515625, -76.61041259765625)
		before = time.time()
		f(lat, lon)
		after = time.time()		
		lookup_time += (after-before)
	avg_time = round(lookup_time / points, 4)
	print " - Took %ss to lookup %s points. Avg of %ss" % (lookup_time, points, avg_time)

def time_near(model, is_mongo=False, points=2500):
	f = lambda lat, lon: model.objects.distance("POINT (%s %s)" % (lon, lat))[:5] if not is_mongo else \
		lambda lat, lon: model.objects(geom__near=[lon, lat])[:5]
	time_geo(model, is_mongo, points, f)

def time_within(model, is_mongo=False, points=2500):
	f = lambda lat, lon: model.objects.filter(geom__contains="POINT (%s %s)" % (lon, lat)) if not is_mongo else \
		lambda lat, lon: model.objects(geom__geo_within=[lon, lat])
	time_geo(model, is_mongo, points, f)

def insert_roads(is_mongo=False):
	# Load roads
	if not is_mongo:
		PsqlRoad.objects.all().delete()
	roads_shp = "%s/data/Roads_ALL_VCS.shp" % settings.PROJECT_ROOT
	ds = DataSource(roads_shp)
	lyr = ds[0][:100000]
	total = len(lyr)
	insertion_time = 0
	for feat in lyr:
		name = feat.get("NAME")
		try:
			geom = feat.geom
		except Exception:
			total -= 1
			continue
		geom_type = geom.geom_type.name
		if is_mongo:
			if geom_type == "LineString":
				points = [[x,y] for (x,y) in geom.coords]
			else:
				points = [[x,y] for (x,y) in geom.coords[0]]
			before = time.time()
			MongoRoad.objects.create(name=name, geom=points)
			after = time.time()
		else:
			wkt = feat.geom.wkt
			if wkt.startswith("LINESTRING"):
				wkt = wkt.replace('LINESTRING (','MULTILINESTRING ((') + ')'
			try:
				before = time.time()
				PsqlRoad.objects.create(name=name, geom=wkt)
				after = time.time()
			except Exception as e:
				print e
				total -= 1
				continue
		insertion_time += (after - before)
	avg_time = round(insertion_time / total, 4)
	print " - Took %ss to insert %s roads, avg of %s per insert" % (insertion_time, total, avg_time)

def run_timing():
	print "Running timing tests.... "
	community_shp = "%s/data/community.shp" % settings.PROJECT_ROOT

	print "Importing communities for psql"
	insertion_time = 0
	ds = DataSource(community_shp)
	layer = ds[0]
	PsqlPolygon.objects.all().delete()
	for feat in layer:
		name = feat.get("COMMUNITY")
		geom = feat.geom.wkt
		if geom.startswith("POLYGON"):
			geom = geom.replace('POLYGON (','MULTIPOLYGON ((') + ')'
		before = time.time()
		# DO the insert
		PsqlPolygon.objects.create(name=name, geom=geom)
		after = time.time()
		insertion_time += (after - before)
	avg_time = round(insertion_time / len(layer), 2)
	print " - Took %ss to insert %s records for PostGIS (%ss per insert)" % (insertion_time, len(layer), avg_time)
	time_within(PsqlPolygon)
	insert_roads()
	time_near(PsqlRoad)

	print
	print "Importing community for mongo"
	insertion_time = 0
	ds = DataSource(community_shp)
	layer = ds[0]
	for feat in layer:
		name = feat.get("COMMUNITY")
		geom = feat.geom
		before = time.time()
		geom_type = geom.geom_type.name
		if geom_type == "Polygon":
			points = [[x,y] for (x,y) in geom.coords[0]]
		else:
			points = [[x,y] for (x,y) in geom.coords[0][0]] # multipolygon
		# DO the insert
		MongoPolygon.objects.create(name=name, geom=[points])
		after = time.time()
		insertion_time += (after - before)
	avg_time = round(insertion_time / len(layer),2)
	print " - Took %ss to insert %s records for Mongo (%s per insert)" % (insertion_time, len(layer), avg_time)
	time_within(MongoPolygon)
	insert_roads(True)
	time_near(MongoRoad, is_mongo=True)
